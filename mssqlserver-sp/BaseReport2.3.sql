USE [test]
GO
/****** Object:  StoredProcedure [dbo].[BaseReport2]    Script Date: 13.02.2018 15:52:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[BaseReport2]
	@PPRICEID [uniqueidentifier],
	@PRODUCTS_ID [uniqueidentifier],
	@PRICEBASE [float]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		case PRICESRECORDS.PRICEID when @PPRICEID then 0 else 1 end as ISBASEPRICES,
		DISTRIBUTORS.NAME as DISTRIBUTORS_NAME,
		PRICES.NAME as PRICES_NAME,
		PRICESRECORDS.NAME as PRODUCTS_NAME,
		PRICESRECORDS.PRICE AS PRICE,
		round(PRICESRECORDS.PRICE - @PRICEBASE ,2) as DIFFERENCE_PRICE
	from LINKS 
		join PRICESRECORDS on PRICESRECORDS.RECORDINDEX = LINKS.PRICERECORDINDEX
		join PRICES on PRICES.ID = PRICESRECORDS.PRICEID
		join DISTRIBUTORS on DISTRIBUTORS.ID = PRICES.DISID
	where
		LINKS.CATALOGPRODUCTID = @PRODUCTS_ID
		and PRICESRECORDS.PRICE <> 0
		and PRICESRECORDS.USED = 1
		and PRICESRECORDS.DELETED = 0
	order by ISBASEPRICES,PRICE
END
