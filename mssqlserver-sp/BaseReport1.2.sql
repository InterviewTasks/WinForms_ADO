USE [test]
GO
/****** Object:  StoredProcedure [dbo].[BaseReport1]    Script Date: 13.02.2018 15:53:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[BaseReport1]
	@PPRICEID [uniqueidentifier],
	@CRITETION int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- параметры товара из выбранного прайса в других прайс-листах
select 
	PRODUCTS.ID AS PRODUCTS_ID,
	PRICESRECORDS.PRICEID AS PRICEID,
	PRICESRECORDS.PRICE AS PRICE
into #T1
from PRICESRECORDS 
	join LINKS on LINKS.PRICERECORDINDEX = PRICESRECORDS.RECORDINDEX
	join PRODUCTS on PRODUCTS.ID = LINKS.CATALOGPRODUCTID
WHERE
	PRICESRECORDS.PRICEID != @PPRICEID
	and PRICESRECORDS.USED = 1
	and PRICESRECORDS.DELETED = 0
	and PRODUCTS.DELETED = 0
	and PRICESRECORDS.PRICE <> 0


-- товар с минимальными значениями и ИД прайс-листа
select 
	ROW_NUMBER() OVER(PARTITION BY #T1.PRODUCTS_ID ORDER BY #T1.PRICEID ASC) AS Row#,
	#T1.PRODUCTS_ID,
	#T1.PRICEID,
	#T1.PRICE AS PRICE_MIN
into #T3
from #T1 
	join 
	(
		-- минимальные значение по товару
		select				
			PRODUCTS_ID,
			MIN(PRICE) AS PRICE_MIN
		from #T1
		GROUP BY PRODUCTS_ID
	)
	AS T2 on #T1.PRODUCTS_ID = T2.PRODUCTS_ID and #T1.PRICE = T2.PRICE_MIN

create NONCLUSTERED index IDX_#T3 on #T3 (PRODUCTS_ID) on [PRIMARY]

create table #R1 (
	PRODUCTS_ID uniqueidentifier NOT NULL,
	PRODUCTS_NAME VARCHAR(2048) NOT NULL,
	PRICE float NOT NULL,
	DISTRIBUTORS_ID uniqueidentifier  NULL,
	DISTRIBUTORS_NAME VARCHAR(255) NULL,
	PRICES_ID uniqueidentifier NULL,
	PRICES_NAME  VARCHAR(255) NULL,
	PRICE_MIN float NULL,
	DIFFERENCE_MIN float NULL,
	NOTE VARCHAR(255) NULL
)

DECLARE  
	@PRODUCTS_ID  uniqueidentifier,
	@PRODUCTS_NAME  VARCHAR(2048),
	@PRICE float,

	@PRICEID uniqueidentifier,
	@DISTRIBUTORS_ID uniqueidentifier,
	@DISTRIBUTORS_NAME VARCHAR(255),
	@PRICES_ID uniqueidentifier,
	@PRICES_NAME  VARCHAR(255),
	@PRICE_MIN float,
	@DIFFERENCE_MIN float,
	@NOTE VARCHAR(255)

DECLARE R1_cursor CURSOR LOCAL FOR
	select 
		PRODUCTS.ID AS PRODUCTS_ID,
		rtrim(ltrim(PRODUCTS.NAME)) + ' ' + rtrim(ltrim(PRODUCTS.CHILDNAME)) AS PRODUCTS_NAME,
		PRICESRECORDS.PRICE AS PRICE
	from PRICESRECORDS 
		join LINKS on LINKS.PRICERECORDINDEX = PRICESRECORDS.RECORDINDEX
		join PRODUCTS on PRODUCTS.ID = LINKS.CATALOGPRODUCTID
	WHERE
		PRICESRECORDS.PRICEID =  @PPRICEID
		and PRICESRECORDS.USED = 1
		and PRICESRECORDS.DELETED = 0
		and PRODUCTS.DELETED = 0

OPEN R1_cursor
FETCH NEXT FROM R1_cursor INTO @PRODUCTS_ID, @PRODUCTS_NAME, @PRICE
WHILE @@FETCH_STATUS=0
BEGIN
	set @PRICEID = null
	set @DISTRIBUTORS_ID = null
	set @DISTRIBUTORS_NAME = null
	set @PRICES_ID = null
	set @PRICES_NAME = null
	set @PRICE_MIN = null
	set @DIFFERENCE_MIN = null
	set @NOTE = null
	
	select 
		@PRICEID = PRICEID,
		@PRICE_MIN = PRICE_MIN,
		@PRICES_NAME = PRICES.NAME,
		@DISTRIBUTORS_ID = DISTRIBUTORS.ID,
		@DISTRIBUTORS_NAME = DISTRIBUTORS.NAME
	from #T3 
		join PRICES on PRICEID = PRICES.ID
		join DISTRIBUTORS on PRICES.DISID = DISTRIBUTORS.ID
	where 
		PRODUCTS_ID = @PRODUCTS_ID 
		and Row# = 1
	
	select @NOTE = isnull(@NOTE + '; ','') + rtrim(ltrim(PRICES.NAME))
	from #T3 
		join PRICES on PRICES.ID = #T3.PRICEID
	where 
		#T3.PRODUCTS_ID = @PRODUCTS_ID 
		and #T3.Row# != 1
	
	if not @PRICE is null and not @PRICE_MIN is null 
		set @DIFFERENCE_MIN = round(@PRICE_MIN - @PRICE, 2)
	if @CRITETION = 1 
		if @DIFFERENCE_MIN >= 0
			begin
				set @PRICE_MIN = null
				set @DIFFERENCE_MIN = null
			end
	insert into #R1 (
		PRODUCTS_ID,
		PRODUCTS_NAME,
		PRICE,
		PRICES_ID,
		PRICES_NAME,
		PRICE_MIN,
		DIFFERENCE_MIN,
		DISTRIBUTORS_ID,
		DISTRIBUTORS_NAME,
		NOTE
		)
		values (
		@PRODUCTS_ID,
		@PRODUCTS_NAME,
		@PRICE,
		@PRICEID,
		@PRICES_NAME,
		@PRICE_MIN,
		@DIFFERENCE_MIN,
		@DISTRIBUTORS_ID,
		@DISTRIBUTORS_NAME,
		@NOTE
		)
	FETCH NEXT FROM R1_cursor 
      INTO @PRODUCTS_ID, @PRODUCTS_NAME, @PRICE
END
CLOSE R1_cursor
DEALLOCATE R1_cursor

SELECT * from #R1

END
