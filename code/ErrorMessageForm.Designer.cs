namespace interview.winforms.ado.net
{
    partial class ErrorMessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorMessageForm));
            this.okButton = new System.Windows.Forms.Button();
            this.DetalButton = new System.Windows.Forms.Button();
            this.TextBoxDetail = new System.Windows.Forms.TextBox();
            this.textBoxShort = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(525, 12);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&�������";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // DetalButton
            // 
            this.DetalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DetalButton.Location = new System.Drawing.Point(525, 58);
            this.DetalButton.Name = "DetalButton";
            this.DetalButton.Size = new System.Drawing.Size(75, 23);
            this.DetalButton.TabIndex = 1;
            this.DetalButton.Text = "&��������";
            this.DetalButton.UseVisualStyleBackColor = true;
            this.DetalButton.Click += new System.EventHandler(this.DetalButton_Click);
            // 
            // TextBoxDetail
            // 
            this.TextBoxDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBoxDetail.Location = new System.Drawing.Point(12, 104);
            this.TextBoxDetail.Multiline = true;
            this.TextBoxDetail.Name = "TextBoxDetail";
            this.TextBoxDetail.ReadOnly = true;
            this.TextBoxDetail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxDetail.Size = new System.Drawing.Size(588, 177);
            this.TextBoxDetail.TabIndex = 2;
            this.TextBoxDetail.TabStop = false;
            // 
            // textBoxShort
            // 
            this.textBoxShort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxShort.Location = new System.Drawing.Point(12, 12);
            this.textBoxShort.Multiline = true;
            this.textBoxShort.Name = "textBoxShort";
            this.textBoxShort.ReadOnly = true;
            this.textBoxShort.Size = new System.Drawing.Size(498, 69);
            this.textBoxShort.TabIndex = 4;
            // 
            // ErrorMessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 293);
            this.Controls.Add(this.textBoxShort);
            this.Controls.Add(this.TextBoxDetail);
            this.Controls.Add(this.DetalButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorMessageForm";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button DetalButton;
        private System.Windows.Forms.TextBox TextBoxDetail;
        private System.Windows.Forms.TextBox textBoxShort;
    }
}