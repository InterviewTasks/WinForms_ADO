﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace interview.winforms.ado.net
{
    public static class ModulesStatic
    {
        public static String FolderDataApp { get; } = Directory.GetCurrentDirectory();
        public static String FileConfigExt { get; } = @".xml";

        public static String FileDbConnect { get; set; } = Path.Combine(FolderDataApp, @"dbconnect" + FileConfigExt);
        public static String DbConnString { get; set; }

        public static Boolean CreateDbStringFromXmlFile()
        {
            Boolean Result = false;
            try
            {
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(FileDbConnect, XmlReadMode.InferSchema);
                DataRow dataRow = dataSet.Tables[0].Rows[0];
                //   FileDbConnect = "Data Source=" + dataRow["server"] + ";Initial Catalog=" + dataRow["db"] + ";Integrated Security=True;";
                DbConnString = String.Format("Server={0};Database={1};Integrated Security=True;", dataRow["server"], dataRow["db"]);
                Result = true;
            }
            catch (Exception ex)
            {
                new Modules().ErrorExceptionApp(ex, @"Неудача при создание строки из файла конфигурации: " + FileDbConnect);
            }
            return Result;
        }

        public static List<Criterion> Criterions
        {
            get
            {
                return
                     new List<Criterion>
                        {
                          new Criterion { Id = CriterionIndex.All,  DisplayValue = "Все"},
                          new Criterion { Id = CriterionIndex.Other,   DisplayValue = "Минимальная цена у других"},
                          new Criterion { Id = CriterionIndex.InHere,   DisplayValue = "Минимальная цена здесь"},
                        };
            }
        }

        public class Criterion
        {
            public CriterionIndex Id { get; set; }
            public String DisplayValue { get; set; }
        }
    }

    public enum CriterionIndex : Int32
    {
        All = 0,
        Other = 1,
        InHere = 2
    }
}
