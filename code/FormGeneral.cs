﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace interview.winforms.ado.net
{
    public partial class FormGeneral : Form
    {
        DataSet ds;
        BindingSource bs_DISTRIBUTORS;
        BindingSource bs_PRICES;
        BindingSource bs_REPORT1;
        BindingSource bs_REPORT2;

        String tableName_PRICES = "PRICES";
        String tableName_DISTRIBUTORS = "DISTRIBUTORS";
        String tableName_REPORT1 = "REPORT1";
        String tableName_REPORT2 = "REPORT2";

        public FormGeneral()
        {
            InitializeComponent();
            // Default
            Size = new Size(900, 600);
        }

        private void FormGeneral_Load(object sender, EventArgs e)
        {
            try
            {
                // первичная загрузка данных в комбобокс DISTRIBUTORS, PRICES
                PrimaryLoad();
                // настраиваем
                InitCombo();
                // настраиваем комбо "Критерий"
                InitCriterion();
                // Формируем гриды
                InitDGVs();
                // создаем  структуру выходных таблиц
                CreateReportTables();
                // назначаем окружение для загрузки в выходные таблицы
                InitReportTables();
            }
            catch (Exception ex)
            {
                new Modules().ErrorExceptionApp(ex, "Ошибка при запуске");
            }
        }

        private void PrimaryLoad()
        {
            ds = new DataSet();

            #region DISTRIBUTORS

            SqlDataAdapter da_DISTRIBUTORS;
            {
                DataTable tbl = ds.Tables.Add(tableName_DISTRIBUTORS);
                DataColumn ID = tbl.Columns.Add("ID", Type.GetType("System.Guid"));
                ID.ReadOnly = true;
                // Set the primary key.
                tbl.PrimaryKey = new DataColumn[] { ID };

                DataColumn NAME = tbl.Columns.Add("NAME", Type.GetType("System.String"));
                NAME.ReadOnly = true;

                DataTableMapping tableMapping = new DataTableMapping();
                tableMapping.SourceTable = "Table";
                tableMapping.DataSetTable = tableName_DISTRIBUTORS;
                tableMapping.ColumnMappings.Add("ID", "ID");
                tableMapping.ColumnMappings.Add("NAME", "NAME");

                da_DISTRIBUTORS = new SqlDataAdapter();
                da_DISTRIBUTORS.ContinueUpdateOnError = false;
                da_DISTRIBUTORS.AcceptChangesDuringUpdate = true;
                da_DISTRIBUTORS.TableMappings.Add(tableMapping);

                string TextCommandSelect =
                    "SELECT " +
                    "ID," +
                    "NAME" +
                    " FROM [" + tableName_DISTRIBUTORS + "]" +
                    " WHERE ACTIVE=1";                                  // только ACTIVE=1
                da_DISTRIBUTORS.SelectCommand = new SqlCommand();
                da_DISTRIBUTORS.SelectCommand.Connection = new SqlConnection();
                da_DISTRIBUTORS.SelectCommand.Connection.ConnectionString = ModulesStatic.DbConnString;
                da_DISTRIBUTORS.SelectCommand.CommandText = TextCommandSelect;
                da_DISTRIBUTORS.SelectCommand.CommandType = CommandType.Text;
            }
            #endregion DISTRIBUTORS

            #region PRICES

            SqlDataAdapter da_PRICES;
            {
                DataTable tbl = ds.Tables.Add(tableName_PRICES);
                DataColumn ID = tbl.Columns.Add("ID", Type.GetType("System.Guid"));
                ID.ReadOnly = true;
                // Set the primary key.
                tbl.PrimaryKey = new DataColumn[] { ID };

                DataColumn NAME = tbl.Columns.Add("NAME", Type.GetType("System.String"));
                NAME.ReadOnly = true;

                DataColumn DISID = tbl.Columns.Add("DISID", Type.GetType("System.Guid"));
                ID.ReadOnly = true;

                DataTableMapping tableMapping = new DataTableMapping();
                tableMapping.SourceTable = "Table";
                tableMapping.DataSetTable = tableName_PRICES;
                tableMapping.ColumnMappings.Add("ID", "ID");
                tableMapping.ColumnMappings.Add("NAME", "NAME");
                tableMapping.ColumnMappings.Add("DISID", "DISID");

                da_PRICES = new SqlDataAdapter();
                da_PRICES.ContinueUpdateOnError = false;
                da_PRICES.AcceptChangesDuringUpdate = true;
                da_PRICES.TableMappings.Add(tableMapping);

                string TextCommandSelect =
                    "SELECT " +
                    "ID," +
                    "NAME," +
                    "DISID" +
                    " FROM [" + tableName_PRICES + "]" +
                    " WHERE " +
                    " EXISTS " +
                    "(" +
                    "select ID from [" + tableName_DISTRIBUTORS + "] WHERE ACTIVE=1 and " +         // только ACTIVE=1
                    "[" + tableName_PRICES + "].DISID = " +
                    "[" + tableName_DISTRIBUTORS + "].ID" +
                    ")";
                ;
                da_PRICES.SelectCommand = new SqlCommand();
                da_PRICES.SelectCommand.Connection = new SqlConnection();
                da_PRICES.SelectCommand.Connection.ConnectionString = ModulesStatic.DbConnString;
                da_PRICES.SelectCommand.CommandText = TextCommandSelect;
                da_PRICES.SelectCommand.CommandType = CommandType.Text;
            }
            #endregion PRICES

            da_DISTRIBUTORS.Fill(ds, tableName_DISTRIBUTORS);
            da_PRICES.Fill(ds, tableName_PRICES);

            bs_DISTRIBUTORS = new BindingSource();
            bs_DISTRIBUTORS.DataSource = ds;
            bs_DISTRIBUTORS.DataMember = tableName_DISTRIBUTORS;

            DataRelation relation = new DataRelation("DISTRIBUTORS_PRICES",
                ds.Tables["DISTRIBUTORS"].Columns["ID"],
                ds.Tables["PRICES"].Columns["DISID"]);
            ds.Relations.Add(relation);

            bs_PRICES = new BindingSource();
            bs_PRICES.DataSource = bs_DISTRIBUTORS;
            bs_PRICES.DataMember = "DISTRIBUTORS_PRICES";

            bs_DISTRIBUTORS.Sort = "NAME";
            bs_PRICES.Sort = "NAME";
        }

        private void InitCombo()
        {
            comboBox_DISTRIBUTORS.DataSource = bs_DISTRIBUTORS;
            comboBox_DISTRIBUTORS.ValueMember = "ID";
            comboBox_DISTRIBUTORS.DisplayMember = "NAME";

            comboBox_PRICES.DataSource = bs_PRICES;
            comboBox_PRICES.ValueMember = "ID";
            comboBox_PRICES.DisplayMember = "NAME";
        }

        private void InitCriterion()
        {
            comboBox_Criterion.DataSource = ModulesStatic.Criterions;
            comboBox_Criterion.ValueMember = "Id";
            comboBox_Criterion.DisplayMember = "DisplayMember";

            // Default
            comboBox_Criterion.SelectedValue = CriterionIndex.All;
        }

        private void InitDGVs()
        {
            dataGridView1.AutoGenerateColumns = false;

            dataGridView1.Columns.Add("PRODUCTS_NAME", "Наименование товара");
            dataGridView1.Columns["PRODUCTS_NAME"].DataPropertyName = "PRODUCTS_NAME";
            dataGridView1.Columns["PRODUCTS_NAME"].ReadOnly = true;
            dataGridView1.Columns["PRODUCTS_NAME"].Visible = true;
            dataGridView1.Columns["PRODUCTS_NAME"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["PRODUCTS_NAME"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dataGridView1.Columns.Add("PRICE", "Цена");
            dataGridView1.Columns["PRICE"].DataPropertyName = "PRICE";
            dataGridView1.Columns["PRICE"].ReadOnly = true;
            dataGridView1.Columns["PRICE"].Visible = true;
            dataGridView1.Columns["PRICE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["PRICE"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["PRICE"].ValueType = Type.GetType("System.Decimal");

            dataGridView1.Columns.Add("PRICE_MIN", "Мин.цена");
            dataGridView1.Columns["PRICE_MIN"].DataPropertyName = "PRICE_MIN";
            dataGridView1.Columns["PRICE_MIN"].ReadOnly = true;
            dataGridView1.Columns["PRICE_MIN"].Visible = true;
            dataGridView1.Columns["PRICE_MIN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["PRICE_MIN"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["PRICE_MIN"].ValueType = Type.GetType("System.Decimal");

            dataGridView1.Columns.Add("DIFFERENCE_MIN", "Разница (Мин)");
            dataGridView1.Columns["DIFFERENCE_MIN"].DataPropertyName = "DIFFERENCE_MIN";
            dataGridView1.Columns["DIFFERENCE_MIN"].ReadOnly = true;
            dataGridView1.Columns["DIFFERENCE_MIN"].Visible = true;
            dataGridView1.Columns["DIFFERENCE_MIN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["DIFFERENCE_MIN"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["DIFFERENCE_MIN"].ValueType = Type.GetType("System.Decimal");

            dataGridView1.Columns.Add("PRICE_MAX", "Макс.цена");
            dataGridView1.Columns["PRICE_MAX"].DataPropertyName = "PRICE_MAX";
            dataGridView1.Columns["PRICE_MAX"].ReadOnly = true;
            dataGridView1.Columns["PRICE_MAX"].Visible = true;
            dataGridView1.Columns["PRICE_MAX"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["PRICE_MAX"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["PRICE_MAX"].ValueType = Type.GetType("System.Decimal");

            dataGridView1.Columns.Add("DIFFERENCE_MAX", "Разница (Макс)");
            dataGridView1.Columns["DIFFERENCE_MAX"].DataPropertyName = "DIFFERENCE_MAX";
            dataGridView1.Columns["DIFFERENCE_MAX"].ReadOnly = true;
            dataGridView1.Columns["DIFFERENCE_MAX"].Visible = true;
            dataGridView1.Columns["DIFFERENCE_MAX"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["DIFFERENCE_MAX"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["DIFFERENCE_MAX"].ValueType = Type.GetType("System.Decimal");

            dataGridView1.Columns.Add("DISTRIBUTORS_NAME", "Поставщик");
            dataGridView1.Columns["DISTRIBUTORS_NAME"].DataPropertyName = "DISTRIBUTORS_NAME";
            dataGridView1.Columns["DISTRIBUTORS_NAME"].ReadOnly = true;
            dataGridView1.Columns["DISTRIBUTORS_NAME"].Visible = true;
            dataGridView1.Columns["DISTRIBUTORS_NAME"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView1.Columns["DISTRIBUTORS_NAME"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dataGridView1.Columns.Add("PRICES_NAME", "Прайс-лист");
            dataGridView1.Columns["PRICES_NAME"].DataPropertyName = "PRICES_NAME";
            dataGridView1.Columns["PRICES_NAME"].ReadOnly = true;
            dataGridView1.Columns["PRICES_NAME"].Visible = true;
            dataGridView1.Columns["PRICES_NAME"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView1.Columns["PRICES_NAME"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dataGridView1.Columns.Add("NOTE", "Примечание");
            dataGridView1.Columns["NOTE"].DataPropertyName = "NOTE";
            dataGridView1.Columns["NOTE"].ReadOnly = true;
            dataGridView1.Columns["NOTE"].Visible = true;
            dataGridView1.Columns["NOTE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView1.Columns["NOTE"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;


            dataGridView2.AutoGenerateColumns = false;

            dataGridView2.Columns.Add("DISTRIBUTORS_NAME", "Поставщик");
            dataGridView2.Columns["DISTRIBUTORS_NAME"].DataPropertyName = "DISTRIBUTORS_NAME";
            dataGridView2.Columns["DISTRIBUTORS_NAME"].ReadOnly = true;
            dataGridView2.Columns["DISTRIBUTORS_NAME"].Visible = true;
            dataGridView2.Columns["DISTRIBUTORS_NAME"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView2.Columns["DISTRIBUTORS_NAME"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView2.Columns["DISTRIBUTORS_NAME"].SortMode = DataGridViewColumnSortMode.NotSortable;

            dataGridView2.Columns.Add("PRICES_NAME", "Прайс-лист");
            dataGridView2.Columns["PRICES_NAME"].DataPropertyName = "PRICES_NAME";
            dataGridView2.Columns["PRICES_NAME"].ReadOnly = true;
            dataGridView2.Columns["PRICES_NAME"].Visible = true;
            dataGridView2.Columns["PRICES_NAME"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView2.Columns["PRICES_NAME"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView2.Columns["PRICES_NAME"].SortMode = DataGridViewColumnSortMode.NotSortable;

            dataGridView2.Columns.Add("PRODUCTS_NAME", "Наименование товара");
            dataGridView2.Columns["PRODUCTS_NAME"].DataPropertyName = "PRODUCTS_NAME";
            dataGridView2.Columns["PRODUCTS_NAME"].ReadOnly = true;
            dataGridView2.Columns["PRODUCTS_NAME"].Visible = true;
            dataGridView2.Columns["PRODUCTS_NAME"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView2.Columns["PRODUCTS_NAME"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridView2.Columns["PRODUCTS_NAME"].SortMode = DataGridViewColumnSortMode.NotSortable;

            dataGridView2.Columns.Add("PRICE", "Цена");
            dataGridView2.Columns["PRICE"].DataPropertyName = "PRICE";
            dataGridView2.Columns["PRICE"].ReadOnly = true;
            dataGridView2.Columns["PRICE"].Visible = true;
            dataGridView2.Columns["PRICE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["PRICE"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView2.Columns["PRICE"].ValueType = Type.GetType("System.Decimal");
            dataGridView2.Columns["PRICE"].SortMode = DataGridViewColumnSortMode.NotSortable;

            dataGridView2.Columns.Add("DIFFERENCE_PRICE", "Разница");
            dataGridView2.Columns["DIFFERENCE_PRICE"].DataPropertyName = "DIFFERENCE_PRICE";
            dataGridView2.Columns["DIFFERENCE_PRICE"].ReadOnly = true;
            dataGridView2.Columns["DIFFERENCE_PRICE"].Visible = true;
            dataGridView2.Columns["DIFFERENCE_PRICE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["DIFFERENCE_PRICE"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView2.Columns["DIFFERENCE_PRICE"].ValueType = Type.GetType("System.Decimal");
            dataGridView2.Columns["DIFFERENCE_PRICE"].SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void CreateReportTables()
        {
            {
                DataTable tbl = ds.Tables.Add(tableName_REPORT1);

                DataColumn PRODUCT_NAME = tbl.Columns.Add("PRODUCTS_NAME", Type.GetType("System.String"));
                DataColumn PRICE = tbl.Columns.Add("PRICE", Type.GetType("System.Decimal"));
                DataColumn PRICE_MIN = tbl.Columns.Add("PRICE_MIN", Type.GetType("System.Decimal"));
                DataColumn DIFFERENCE_MIN = tbl.Columns.Add("DIFFERENCE_MIN", Type.GetType("System.Decimal"));
                DataColumn PRICE_MAX = tbl.Columns.Add("PRICE_MAX", Type.GetType("System.Decimal"));
                DataColumn DIFFERENCE_MAX = tbl.Columns.Add("DIFFERENCE_MAX", Type.GetType("System.Decimal"));
                DataColumn DISTRIBUTOR = tbl.Columns.Add("DISTRIBUTOR_NAME", Type.GetType("System.String"));
                DataColumn PRICES = tbl.Columns.Add("PRICES", Type.GetType("System.String"));
                DataColumn NOTE = tbl.Columns.Add("NOTE", Type.GetType("System.String"));

                DataColumn PRODUCT_ID = tbl.Columns.Add("PRODUCT_ID", Type.GetType("System.Guid"));
                DataColumn DISTRIBUTOR_ID = tbl.Columns.Add("DISTRIBUTOR_ID", Type.GetType("System.Guid"));
                DataColumn PRICES_ID = tbl.Columns.Add("PRICES_ID", Type.GetType("System.Guid"));
            }
            {
                DataTable tbl = ds.Tables.Add(tableName_REPORT2);

                DataColumn ISBASEPRICES = tbl.Columns.Add("ISBASEPRICES", Type.GetType("System.Int32"));
                DataColumn DISTRIBUTORS_NAME = tbl.Columns.Add("DISTRIBUTORS_NAME", Type.GetType("System.String"));
                DataColumn PRICES_NAME = tbl.Columns.Add("PRICES_NAME", Type.GetType("System.String"));
                DataColumn PRODUCTS_NAME = tbl.Columns.Add("PRODUCTS_NAME", Type.GetType("System.String"));
                DataColumn PRICE = tbl.Columns.Add("PRICE", Type.GetType("System.Decimal"));
                DataColumn DIFFERENCE_PRICE = tbl.Columns.Add("DIFFERENCE_PRICE", Type.GetType("System.Decimal"));
            }
        }

        private SqlDataAdapter da_REPORT1;
        private SqlDataAdapter da_REPORT2;

        private void InitReportTables()
        {
            {
                DataTableMapping tableMapping = new DataTableMapping();
                tableMapping.SourceTable = "Table";
                tableMapping.DataSetTable = tableName_REPORT1;
                tableMapping.ColumnMappings.Add("PRODUCTS_ID", "PRODUCTS_ID");
                tableMapping.ColumnMappings.Add("PRODUCTS_NAME", "PRODUCTS_NAME");
                tableMapping.ColumnMappings.Add("PRICE", "PRICE");
                tableMapping.ColumnMappings.Add("PRICE_MIN", "PRICE_MIN");
                tableMapping.ColumnMappings.Add("DIFFERENCE_MIN", "DIFFERENCE_MIN");
                tableMapping.ColumnMappings.Add("DISTRIBUTORS_ID", "DISTRIBUTORS_ID");
                tableMapping.ColumnMappings.Add("DISTRIBUTORS_NAME", "DISTRIBUTORS_NAME");
                tableMapping.ColumnMappings.Add("PRICES_ID", "PRICES_ID");
                tableMapping.ColumnMappings.Add("PRICES_NAME", "PRICES_NAME");
                tableMapping.ColumnMappings.Add("NOTE", "NOTE");

                da_REPORT1 = new SqlDataAdapter();
                da_REPORT1.ContinueUpdateOnError = false;
                da_REPORT1.AcceptChangesDuringUpdate = true;
                da_REPORT1.TableMappings.Add(tableMapping);

                string TextCommandSelect = "BaseReport1";
                da_REPORT1.SelectCommand = new SqlCommand();
                da_REPORT1.SelectCommand.Connection = new SqlConnection();
                da_REPORT1.SelectCommand.Connection.ConnectionString = ModulesStatic.DbConnString;
                da_REPORT1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da_REPORT1.SelectCommand.CommandText = TextCommandSelect;

                da_REPORT1.SelectCommand.Parameters.Add(new SqlParameter("@PPRICEID", SqlDbType.UniqueIdentifier));
                da_REPORT1.SelectCommand.Parameters.Add(new SqlParameter("@CRITETION", SqlDbType.Int));

                bs_REPORT1 = new BindingSource();
                bs_REPORT1.DataSource = ds;
                bs_REPORT1.DataMember = tableName_REPORT1;
                dataGridView1.DataSource = bs_REPORT1;
            }
            {
                DataTableMapping tableMapping = new DataTableMapping();
                tableMapping.SourceTable = "Table";
                tableMapping.DataSetTable = tableName_REPORT2;
                tableMapping.ColumnMappings.Add("ISBASEPRICES", "ISBASEPRICES");
                tableMapping.ColumnMappings.Add("DISTRIBUTORS_NAME", "DISTRIBUTORS_NAME");
                tableMapping.ColumnMappings.Add("PRICES_NAME", "PRICES_NAME");
                tableMapping.ColumnMappings.Add("PRODUCTS_NAME", "PRODUCTS_NAME");
                tableMapping.ColumnMappings.Add("PRICE", "PRICE");
                tableMapping.ColumnMappings.Add("DIFFERENCE_PRICE", "DIFFERENCE_PRICE");

                da_REPORT2 = new SqlDataAdapter();
                da_REPORT2.ContinueUpdateOnError = false;
                da_REPORT2.AcceptChangesDuringUpdate = true;
                da_REPORT2.TableMappings.Add(tableMapping);

                string TextCommandSelect = "BaseReport2";
                da_REPORT2.SelectCommand = new SqlCommand();
                da_REPORT2.SelectCommand.Connection = new SqlConnection();
                da_REPORT2.SelectCommand.Connection.ConnectionString = ModulesStatic.DbConnString;
                da_REPORT2.SelectCommand.CommandType = CommandType.StoredProcedure;
                da_REPORT2.SelectCommand.CommandText = TextCommandSelect;

                da_REPORT2.SelectCommand.Parameters.Add(new SqlParameter("@PPRICEID", SqlDbType.UniqueIdentifier));
                da_REPORT2.SelectCommand.Parameters.Add(new SqlParameter("@PRODUCTS_ID", SqlDbType.UniqueIdentifier));
                da_REPORT2.SelectCommand.Parameters.Add(new SqlParameter("@PRICEBASE", SqlDbType.Decimal));

                bs_REPORT2 = new BindingSource();
                bs_REPORT2.DataSource = ds;
                bs_REPORT2.DataMember = tableName_REPORT2;
                dataGridView2.DataSource = bs_REPORT2;
            }
        }

        // Нужно созданить параметры отчета, а то можно поменять с UI, но подготовку не запустить
        private Guid selected_PRICES_ID = new Guid();

        private void button_ExecuteAnalysis_Click(object sender, EventArgs e)
        {
            if (!backgroundWorkerExecute.IsBusy)
            {
                try
                {
                    // Очистка
                    ds.Tables[tableName_REPORT2].Rows.Clear();
                    ds.Tables[tableName_REPORT1].Rows.Clear();
                    if (comboBox_PRICES.SelectedValue != null && comboBox_PRICES.SelectedValue.GetType() == typeof(Guid))
                    {
                        da_REPORT1.SelectCommand.Parameters["@PPRICEID"].Value = comboBox_PRICES.SelectedValue;
                        da_REPORT1.SelectCommand.Parameters["@CRITETION"].Value = (int)comboBox_Criterion.SelectedValue;

                        selected_PRICES_ID = (Guid)comboBox_PRICES.SelectedValue;
                        // уводим в другой поток

                        LockControl(true);

                        bs_REPORT1.DataSource = null;
                        bs_REPORT1.DataMember = "";
                        backgroundWorkerExecute.RunWorkerAsync();
                    }
                    else
                        new Modules().ErrorExceptionApp(new Exception(), "Не выбран прайс-лист");
                }
                catch (Exception ex)
                {
                    new Modules().ErrorExceptionApp(ex, "Ошибка при запуске <Выполнение анализа>");
                }
            }
        }

        private void LockControl(Boolean _lock)
        {
            comboBox_Criterion.Enabled = !_lock;
            comboBox_DISTRIBUTORS.Enabled = !_lock;
            comboBox_PRICES.Enabled = !_lock;
            button_ExecuteAnalysis.Enabled = !_lock;

            dataGridView1.Enabled = !_lock;
            dataGridView2.Enabled = !_lock;
        }

        private void backgroundWorkerExecute_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                da_REPORT1.Fill(ds, tableName_REPORT1);
            }
            catch (Exception ex)
            {
                new Modules().ErrorExceptionApp(ex, "Ошибка получении данных для основной таблицы");
            }
        }


        private void backgroundWorkerExecute_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                bs_REPORT1.Position = bs_REPORT1.Count - 1;
                bs_REPORT1.Position = 0;
                LockControl(false);
                bs_REPORT1.DataSource = ds;
                bs_REPORT1.DataMember = tableName_REPORT1;
            }
            catch (Exception ex)
            {
                new Modules().ErrorExceptionApp(ex, "Ошибка при завершении <Выполнение анализа>");
            }

        }

        // Цвет 1
        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            try
            {
                if (e.ColumnIndex == 3)
                {
                    if (e.Value.GetType().FullName != "System.DBNull")
                    {
                        if (Convert.ToDecimal(e.Value) < 0)
                        {
                            if ((CriterionIndex)comboBox_Criterion.SelectedValue == CriterionIndex.InHere)
                                dataGridView1.Rows[e.RowIndex].Cells[1].Style.ForeColor = Color.Red;
                            else
                            {
                                dataGridView1.Rows[e.RowIndex].Cells[2].Style.ForeColor = Color.Green;
                                dataGridView1.Rows[e.RowIndex].Cells[1].Style.ForeColor = Color.Red;
                            }
                        }
                        else
                        {
                            if ((CriterionIndex)comboBox_Criterion.SelectedValue == CriterionIndex.InHere)
                                dataGridView1.Rows[e.RowIndex].Cells[1].Style.ForeColor = Color.Green;
                            e.Value = "+" + e.Value.ToString();
                        }

                    }
                    else
                    {
                        if ((CriterionIndex)comboBox_Criterion.SelectedValue == CriterionIndex.InHere)
                            dataGridView1.Rows[e.RowIndex].Cells[1].Style.ForeColor = Color.Green;
                    }
                }
            }
            catch (Exception ex)
            {
                new Modules().ErrorExceptionApp(ex, "Ошибка форматировании основной таблицы");
            }
        }

        // Цвет 2
        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.RowIndex == 0)
                    dataGridView2.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightYellow;
                else
                {
                    if (e.ColumnIndex == 4)
                        if (e.Value.GetType().FullName != "System.DBNull")
                            if (Convert.ToDecimal(e.Value) < 0)
                                dataGridView2.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                new Modules().ErrorExceptionApp(ex, "Ошибка форматировании дополнительной таблицы");
            }
        }

        // при изменениии строки
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                // Очистка
                ds.Tables[tableName_REPORT2].Rows.Clear();

                DataRowView drv = bs_REPORT1.Current as DataRowView;
                if (drv != null)
                {
                    da_REPORT2.SelectCommand.Parameters["@PPRICEID"].Value = selected_PRICES_ID;
                    da_REPORT2.SelectCommand.Parameters["@PRODUCTS_ID"].Value = drv["PRODUCTS_ID"];
                    da_REPORT2.SelectCommand.Parameters["@PRICEBASE"].Value = drv["PRICE"];

                    da_REPORT2.Fill(ds, tableName_REPORT2);

                    bs_REPORT2.Sort = "ISBASEPRICES,PRICE";
                }
            }
            catch (Exception ex)
            {
                new Modules().ErrorExceptionApp(ex, "Ошибка получении данных для дополнительной таблицы");
            }
        }
    }
}
