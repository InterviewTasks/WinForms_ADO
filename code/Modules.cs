﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace interview.winforms.ado.net
{
    public class Modules
    {
        public Modules() { }
        ~Modules() { }

        public void ErrorExceptionApp(Exception e, string shortMessage)
        {
            ErrorMessageForm oErrorMessageForm = new ErrorMessageForm(e, shortMessage);
            oErrorMessageForm.ShowDialog();
            oErrorMessageForm.Close();
        }

        public Boolean CheckDbConnect()
        {
            Boolean Result = false;
            using (SqlConnection ConnectApp = new SqlConnection(ModulesStatic.DbConnString))
            {
                SqlCommand SqlCommandApp = ConnectApp.CreateCommand();
                SqlCommandApp.CommandTimeout = 0;
                SqlCommandApp.CommandType = CommandType.Text;
                SqlCommandApp.CommandText = "select top 1 ID from DISTRIBUTORS";
                ConnectApp.Open();
                SqlCommandApp.ExecuteScalar();
                ConnectApp.Close();
                Result = true;
            }
            return Result;
        }
    }
}
