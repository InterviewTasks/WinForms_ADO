﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace interview.winforms.ado.net
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Boolean Success = false;
            if  (ModulesStatic.CreateDbStringFromXmlFile())
            {
                try
                {
                    Success = (new Modules().CheckDbConnect());
                }
                catch (Exception ex)
                {
                    new Modules().ErrorExceptionApp(ex, "Ошибка при проверке соединения с базой данных");
                }
            }
            if (Success)
            {
                Application.Run(new FormGeneral());
            }
            Application.Exit();
        }
    }
}