using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace interview.winforms.ado.net
{
    public partial class ErrorMessageForm : Form
    {
        private int ViewType = 0;
        private int HeightShort = 0;
        private int HeightDetail = 0;

        public ErrorMessageForm(Exception e, String shortMessage)
        {
            InitializeComponent();

            textBoxShort.Text = ErrorIdentification(shortMessage, e);

            HeightDetail = Size.Height;
            HeightShort = textBoxShort.Location.Y + textBoxShort.Size.Height + 45;
            
            // ������
            ViewType = 0;
            SetViewType(this.ViewType);
        }

        private string ErrorIdentification(string ShortMessage, Exception e)
        {
            string ret = ShortMessage;
            if (e.GetType().ToString() == "System.Data.SqlClient.SqlException")
            {
                System.Data.SqlClient.SqlException es = e as System.Data.SqlClient.SqlException;
                TextBoxDetail.Text = es.Number.ToString() + System.Environment.NewLine + es.ToString();
                if (es.Number == 2627)
                {
                    ret = ret + System.Environment.NewLine +
                        "������� � ����� �������������/������ ��� ����������";
                }
                if (es.Number == 547)
                {
                    ret = ret + System.Environment.NewLine +
                        "��������� ������� ������������ � ������ �������";
                }
            }
            else
            {
                TextBoxDetail.Text = e.ToString();
            }
            return ret;
        }

        public void SetViewType(int iViewType)
        {
            if (iViewType == 0)
            {
                Height = HeightShort;
                DetalButton.Text = "��������";
            }
            else
            {
                Height = HeightDetail;
                DetalButton.Text = "������";
            }
        }

        private void DetalButton_Click(object sender, EventArgs e)
        {
            if (this.ViewType == 0)
            {
                this.ViewType = 1;
            }
            else
            {
                this.ViewType = 0;
            }
            this.SetViewType(this.ViewType);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}