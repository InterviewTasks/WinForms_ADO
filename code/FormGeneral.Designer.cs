﻿namespace interview.winforms.ado.net
{
    partial class FormGeneral
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBox_Criterion = new System.Windows.Forms.ComboBox();
            this.label_Criterion = new System.Windows.Forms.Label();
            this.comboBox_PRICES = new System.Windows.Forms.ComboBox();
            this.label_PRICES = new System.Windows.Forms.Label();
            this.comboBox_DISTRIBUTORS = new System.Windows.Forms.ComboBox();
            this.label_DISTRIBUTORS = new System.Windows.Forms.Label();
            this.button_ExecuteAnalysis = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.backgroundWorkerExecute = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(592, 373);
            this.splitContainer1.SplitterDistance = 75;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.button_ExecuteAnalysis);
            this.splitContainer3.Panel2.Padding = new System.Windows.Forms.Padding(10);
            this.splitContainer3.Size = new System.Drawing.Size(592, 75);
            this.splitContainer3.SplitterDistance = 455;
            this.splitContainer3.TabIndex = 7;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.94971F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.05029F));
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Criterion, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_Criterion, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_PRICES, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_PRICES, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_DISTRIBUTORS, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_DISTRIBUTORS, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(455, 75);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // comboBox_Criterion
            // 
            this.comboBox_Criterion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboBox_Criterion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Criterion.FormattingEnabled = true;
            this.comboBox_Criterion.Location = new System.Drawing.Point(80, 53);
            this.comboBox_Criterion.Name = "comboBox_Criterion";
            this.comboBox_Criterion.Size = new System.Drawing.Size(372, 21);
            this.comboBox_Criterion.TabIndex = 2;
            // 
            // label_Criterion
            // 
            this.label_Criterion.AutoSize = true;
            this.label_Criterion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label_Criterion.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label_Criterion.Location = new System.Drawing.Point(3, 56);
            this.label_Criterion.Name = "label_Criterion";
            this.label_Criterion.Padding = new System.Windows.Forms.Padding(0, 0, 0, 6);
            this.label_Criterion.Size = new System.Drawing.Size(71, 19);
            this.label_Criterion.TabIndex = 5;
            this.label_Criterion.Text = "Критерий:";
            this.label_Criterion.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // comboBox_PRICES
            // 
            this.comboBox_PRICES.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboBox_PRICES.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PRICES.FormattingEnabled = true;
            this.comboBox_PRICES.Location = new System.Drawing.Point(80, 28);
            this.comboBox_PRICES.Name = "comboBox_PRICES";
            this.comboBox_PRICES.Size = new System.Drawing.Size(372, 21);
            this.comboBox_PRICES.TabIndex = 1;
            // 
            // label_PRICES
            // 
            this.label_PRICES.AutoSize = true;
            this.label_PRICES.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label_PRICES.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label_PRICES.Location = new System.Drawing.Point(3, 31);
            this.label_PRICES.Name = "label_PRICES";
            this.label_PRICES.Padding = new System.Windows.Forms.Padding(0, 0, 0, 6);
            this.label_PRICES.Size = new System.Drawing.Size(71, 19);
            this.label_PRICES.TabIndex = 6;
            this.label_PRICES.Text = "Прайс-лист:";
            this.label_PRICES.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // comboBox_DISTRIBUTORS
            // 
            this.comboBox_DISTRIBUTORS.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboBox_DISTRIBUTORS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_DISTRIBUTORS.FormattingEnabled = true;
            this.comboBox_DISTRIBUTORS.Location = new System.Drawing.Point(80, 3);
            this.comboBox_DISTRIBUTORS.Name = "comboBox_DISTRIBUTORS";
            this.comboBox_DISTRIBUTORS.Size = new System.Drawing.Size(372, 21);
            this.comboBox_DISTRIBUTORS.TabIndex = 0;
            // 
            // label_DISTRIBUTORS
            // 
            this.label_DISTRIBUTORS.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label_DISTRIBUTORS.Location = new System.Drawing.Point(3, 3);
            this.label_DISTRIBUTORS.Name = "label_DISTRIBUTORS";
            this.label_DISTRIBUTORS.Padding = new System.Windows.Forms.Padding(0, 0, 0, 6);
            this.label_DISTRIBUTORS.Size = new System.Drawing.Size(71, 22);
            this.label_DISTRIBUTORS.TabIndex = 4;
            this.label_DISTRIBUTORS.Text = "Поставщик:";
            this.label_DISTRIBUTORS.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // button_ExecuteAnalysis
            // 
            this.button_ExecuteAnalysis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_ExecuteAnalysis.Location = new System.Drawing.Point(10, 10);
            this.button_ExecuteAnalysis.Name = "button_ExecuteAnalysis";
            this.button_ExecuteAnalysis.Size = new System.Drawing.Size(113, 55);
            this.button_ExecuteAnalysis.TabIndex = 3;
            this.button_ExecuteAnalysis.Text = "Выполнить анализ";
            this.button_ExecuteAnalysis.UseVisualStyleBackColor = true;
            this.button_ExecuteAnalysis.Click += new System.EventHandler(this.button_ExecuteAnalysis_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView1);
            this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridView2);
            this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.splitContainer2.Size = new System.Drawing.Size(592, 294);
            this.splitContainer2.SplitterDistance = 143;
            this.splitContainer2.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(2, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(584, 139);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(2, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(584, 143);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView2_CellFormatting);
            // 
            // backgroundWorkerExecute
            // 
            this.backgroundWorkerExecute.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerExecute_DoWork);
            this.backgroundWorkerExecute.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerExecute_RunWorkerCompleted);
            // 
            // FormGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 373);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "FormGeneral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Анализ прайс-листов";
            this.Load += new System.EventHandler(this.FormGeneral_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label_PRICES;
        private System.Windows.Forms.Label label_Criterion;
        private System.Windows.Forms.Label label_DISTRIBUTORS;
        private System.Windows.Forms.ComboBox comboBox_Criterion;
        private System.Windows.Forms.ComboBox comboBox_PRICES;
        private System.Windows.Forms.ComboBox comboBox_DISTRIBUTORS;
        private System.Windows.Forms.Button button_ExecuteAnalysis;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.ComponentModel.BackgroundWorker backgroundWorkerExecute;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

